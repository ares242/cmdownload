package com.jvg.wildCM;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.message.AbstractHttpMessage;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AsyncDataDownloader {

	static private final String USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36";
    private static final Logger logger = LogManager.getLogger(AsyncDataDownloader.class.getName());
    CookieStore cookieStore = new BasicCookieStore();
    //private CloseableHttpAsyncClient httpClient;
    private CloseableHttpClient httpClient;
    private HttpContext httpContext = HttpClientContext.create();

	public AsyncDataDownloader(){
        int dafaultTimeOut = 100000000;
        RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(dafaultTimeOut)
                .setConnectTimeout(dafaultTimeOut)
                .setConnectionRequestTimeout(dafaultTimeOut)
                .setRedirectsEnabled(true).build();
        //httpClient = HttpAsyncClients.custom()
        httpClient = HttpClients.custom()
                .setDefaultRequestConfig(requestConfig)
	            .setRedirectStrategy(new LaxRedirectStrategy())
	            .build();
        httpContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
	}

    public CloseableHttpClient getHttpClient() {
        return httpClient;
	}

    public void setHttpClient(CloseableHttpClient httpClient) {
        this.httpClient = httpClient;
	}

	public AbstractHttpMessage buildRequest(AbstractHttpMessage request){

		request.setHeader("User-Agent", USER_AGENT);
		request.setHeader("Connection","close");
		request.setHeader("Accept-Encoding","UTF-8");
		return request;
	}

    private InputStream doRequest(HttpRequestBase request)
    {
        try {
            HttpResponse response;
            //Future<HttpResponse> future = httpClient.execute(request, httpContext, null);
            //response = future.get();
            response = httpClient.execute(request, httpContext);
            int responseCode = response.getStatusLine().getStatusCode();
            logger.debug("Response status :" + responseCode);

            if (responseCode != HttpStatus.SC_OK){
                return null;
            }
            //request.releaseConnection();
            return response.getEntity().getContent();
        } /*catch (InterruptedException e) {
            logger.catching(e);
        } catch (ExecutionException e) {
            logger.catching(e);
        }*/ catch (IOException e) {
            logger.catching(e);
        }
        return null;
    }

    private String doRequestString(HttpRequestBase request) {
        try {
            InputStream data = doRequest(request);
            if (data != null)
                return IOUtils.toString(data, "ISO-8859-1");
            request.releaseConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private byte[] doRequestBytes(HttpRequestBase request) {
        try {
            InputStream data = doRequest(request);
            if (data != null)
                return IOUtils.toByteArray(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private HttpGet builtGetRequest(String url) {

        HttpGet request = new HttpGet(url);
        request = (HttpGet) buildRequest(request);

        logger.debug("Sending GET request to " + url);
        return request;
    }

    public String doGetString(String url) {
        return doRequestString(builtGetRequest(url));
    }

    public byte[] doGetBytes(String url) {
        return doRequestBytes(builtGetRequest(url));
    }

    public String doPostString(String url, Map<String, String> postParams)
    {
        HttpPost request = new HttpPost(url);
        request = (HttpPost) buildRequest(request);
        
        Set<Map.Entry<String, String>> entries = postParams.entrySet();
        List<NameValuePair> _postParams = new ArrayList<>();
        entries.forEach(e -> _postParams.add(new BasicNameValuePair(e.getKey(),e.getValue())));
        
        try {
            request.setEntity(new UrlEncodedFormEntity(_postParams));
        } catch (UnsupportedEncodingException e) {
            logger.catching(e);
        }

        logger.debug("Sending POST request to " + url);
        return doRequestString(request);
    }

    
    public void close(){
        try {
            getHttpClient().close();
        } catch (IOException e) {
            logger.catching(e);
        }
    }

    byte[] downloadFile(String url) {
        //URL website;
        //try {
        //website = new URL(url);
        return doGetBytes(url); //IOUtils.toByteArray(website.openStream());
        /*} catch (MalformedURLException e) {
            logger.catching(e);
		} catch (IOException e) {
            logger.catching(e);
		}*/
        //return null;
    }
	
}
