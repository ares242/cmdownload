package com.jvg.wildCM;

import com.jvg.wildCM.model.CMCategory;
import com.jvg.wildCM.model.CMProduct;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.util.*;

public class CMueService {

    static final Logger logger = LogManager.getLogger(CMueService.class.getName());
    static final String urlBase = "http://zonasegura.seace.gob.pe";
    static final String urlPattern = "/portlet5openAjax.asp?_portletid_=mod_cm_p007_plazo_entrega" +
            "&scriptdo=doViewResult&cboCatalogos=1&cboDepartamento3=%s&cboCategoria=%s" +
            "&v_time=20%%20:%%202%%20:%%2017";
    static final String urlPatternSet = "/portlet5openAjax.asp?_portletid_=mod_cm_p007_plazo_entrega" +
            "&scriptdo=doUpdateOcurr&cboConvenio=1&cboDepartamento3=%s&ag_ncor_proveedor=%s" +
            "&ag_plazo=%d&ag_ncor_ficha=%s&v_pagina_actual=1&v_time=23%%20:%%2031%%20:%%2026";
    static final String loginUrl = "/portal5ES.asp?_page_=489.00000";
    static final String priceProductPageUrl = "/portlet5openAjax.asp?_portletid_=mod_cm_p001_modificar_precio" +
            "&scriptdo=doResult&ag_ncor_cat=1&ag_ncor_categoria=%s&ag_prod=%s&v_time=%s";
    static SimpleDateFormat sdf = new SimpleDateFormat("HH%20:%20mm%20:%20ss");

    AsyncDataDownloader downloader = new AsyncDataDownloader();
    String user;

    public CMueService(){
        //downloader.getHttpClient().start();
    }

    public boolean Login(String user, String password)
    {
        this.user = user;
        String data;
        downloader.doGetString(urlBase + "/default.asp");
        downloader.doGetString(urlBase + "/portal5ES.asp?_page_=489.00000");


        Map<String,String> postParams = new LinkedHashMap<>();
        postParams.put("_portletid_", "USP_LOGIN_TO_PORTAL");
        postParams.put("_page_", "489.00000");
        postParams.put("scriptdo", "");
        postParams.put("ag_userid", user);
        postParams.put("ag_password", password);

        data = downloader.doPostString(urlBase + loginUrl, postParams);

        return (data != null && verifyLogin(Jsoup.parse(data)));
    }

    boolean verifyLogin(Document doc) {
        Elements links = doc.getElementsByAttributeValue("href", "?_page_=409.00000");
        return !links.isEmpty();
    }

    public void SetDeliveryTime(Map<String, Integer> daysByRegion, Integer defaultDays  )
    {
        String url = urlBase + "/portal5ES.asp?_page_=420.40900";
        downloader.doGetString(url);

        Map<String,String> postParams = new LinkedHashMap<>();
        postParams.put("_portletid_","mod_cm_p007_plazo_entrega");
        postParams.put("_page_", "420.40900");
        postParams.put("scriptdo", "doViewPlazoEntrega");
        postParams.put("v_ncor_proveedor", "48");
        postParams.put("cboConvenio", "");
        postParams.put("txtPlazo", "");
        postParams.put("cboCatalogos","1");

        String data = downloader.doPostString(url, postParams);
        
        if(data == null || data.isEmpty())
        {
            logger.fatal("No data!");
            downloader.close();
            return;
        }
        Document doc = Jsoup.parse(data);
        
        /*Element catalogSelect =  doc.getElementsByAttributeValue("name","cboCatalogos").first();
        Elements catalogsOp = catalogSelect.getElementsByTag("option");
        List<String> catalogs = new ArrayList<>(catalogsOp.size());
        System.out.println("# Catalogos : "+catalogsOp.size());*/
        
        Element departmentSelect = doc.getElementsByAttributeValue("name", "cboDepartamento3").first();
        Elements departOps = departmentSelect.getElementsByTag("option");
        List<String> departments = new ArrayList<>(departOps.size());
        departOps.forEach(e -> {
            String depID = e.attr("value");
            if(!depID.isEmpty())
                departments.add(depID);
        });
        System.out.println("# Departamentos : " + departOps.size());
        System.out.println(departments);
        
        Element categorySelect = doc.getElementsByAttributeValue("name", "cboCategoria").first();
        Elements catOps = categorySelect.getElementsByTag("option");
        List<String> categories = new ArrayList<>(catOps.size());
        catOps.forEach(e -> {
            String catID = e.attr("value");
            if(!catID.isEmpty())
                categories.add(catID);
        });
        System.out.println("# Categories : "+catOps.size());
        System.out.println(categories);

        String depId = departments.get(0);
        for(String catId : categories)
        {
            String urld = String.format(urlPattern,depId,catId);
            data = downloader.doGetString(urlBase + urld);
            if (data == null)
            {
                System.out.println("Error.");
                continue;
            }
            doc = Jsoup.parse(data);
            int s = getNumberOfPages(doc);
            System.out.println("Number of pages found => "+s);
            doSetDeliveryTime(doc,departments,daysByRegion,defaultDays);
            for(int i = 1; i<s; i++)
            {
                data = downloader.doGetString(urlBase + urld + "&v_pagina_actual=" + (i + 1));
                if (data == null)
                {
                    System.out.println("Error.");
                    continue;
                }
                doc = Jsoup.parse(data);
                doSetDeliveryTime(doc,departments,daysByRegion,defaultDays);
            }
        }
    }
    
    void doSetDeliveryTime(Document doc,List<String> deps,Map<String,Integer> days, Integer defaulDays)
    {
        Elements buttons = doc.getElementsByAttributeValue("value","Enviar");
        System.out.println(buttons.size());
        buttons.forEach(b -> {
            String func = b.attr("onclick");
            func = func.substring(10,func.length()-1);
            String [] params = func.split(",");
            for(String d: deps) {
                Integer p = (days.get(d)==null)?defaulDays:days.get(d);
                String url = urlBase + String.format(urlPatternSet,d,params[5],p,params[7]);
                downloader.doGetString(url);
            }
        });
    }
    
    int getNumberOfPages(Document doc)
    {
        Elements pages = doc.getElementsByTag("table").first()
                .getElementsByAttributeValue("align", "right");
        if (pages.size() != 0) {
            Elements pageLinks_ = pages.first().getElementsByTag("a");
            return pageLinks_.size() + 1;
        } else return 0;
    }

    public void end() {
        downloader.close();
    }

    public List<CMCategory> getCmCategories() {
        //Initial page
        downloader.doGetString(urlBase + "/portal5ES.asp?_page_=363.00000");

        //getting provider ID
        String data = downloader.doGetString(urlBase + "/portal5ES.asp?_page_=415.40900");
        Document doc = Jsoup.parse(data);
        String provId = getProviderId(doc);

        //Getting categories list
        data = downloader.doGetString(urlBase + "/portlet5openAjax.asp?_portletid_=mod_cm_general&scriptdo=doSearchCatxCat" +
                "&ag_ncor_cat=1&ag_ncor_proveedor=" + provId);
        doc = Jsoup.parse(data);
        Element catSelect = doc.getElementById("cboCategoria");
        Elements catOptions = catSelect.getElementsByTag("option");

        List<CMCategory> result = new ArrayList<>(catOptions.size());
        CMCategory cat;
        for (Element inputElement : catOptions) {
            String catID = inputElement.attr("value");
            if (catID.isEmpty())
                continue;
            cat = new CMCategory();
            cat.setCmID(catID);
            cat.setName(inputElement.html());
            result.add(cat);
        }

        return result;
    }

    public List<CMProduct> getCMProductsByCategory(CMCategory cat) {
        List<CMProduct> result = new LinkedList<>();
        List<CMProduct> p;
        String data;
        Document doc;
        String url = String.format(priceProductPageUrl, cat.getCmID(), "&av_ncor_proveedor=" + user + "&v_pagina_actual=" + 1, sdf.format(new Date()));
        data = downloader.doGetString(urlBase + url);
        if (data == null) {
            logger.error("Error downloading category: " + cat.getName() + "(" + cat.getCmID() + "), page: 1");
            return null;
        }
        doc = Jsoup.parse(data);
        int numPages = getNumberOfPages(doc);
        if (numPages == 0)
            return result;
        result = doGetPageCMproducPrices(doc, cat.getName());
        for (int i = 1; i < numPages; i++) {
            url = String.format(priceProductPageUrl, cat.getCmID(), "&av_ncor_proveedor=" + user + "&v_pagina_actual=" + (i + 1), sdf.format(new Date()));
            data = downloader.doGetString(urlBase + url);
            if (data == null) {
                logger.error("Error downloading category: " + cat.getName() + "(" + cat.getCmID() + "), page: " + (i + 1));
                continue;
            }
            doc = Jsoup.parse(data);
            p = doGetPageCMproducPrices(doc, cat.getName());
            result.addAll(p);
        }
        return result;
    }

    public List<CMProduct> getAllCMProducts() {

        List<CMProduct> result = new LinkedList<>();
        List<CMCategory> categories = getCmCategories();
        List<CMProduct> p;
        int totalCats = 0;
        int c = 0;
        for (CMCategory cat : categories) {
            p = getCMProductsByCategory(cat);
            logger.info(cat.getName() + " : " + p.size() + " registros (" + (int) (((double) c++ / (double) categories.size()) * 100) + "%)");
            if (p.size() == 0) continue;
            result.addAll(p);
            totalCats++;
        }
        logger.info("Total : " + result.size() + " productos en " + totalCats + "/" + categories.size() + " categorias");
        return result;
    }

    List<CMProduct> doGetPageCMproducPrices(Document doc, String catName) {

        List<CMProduct> result = new LinkedList<>();
        CMProduct product;
        Element prodsTable = doc.getElementById("idtableItems");
        Elements rows = prodsTable.getElementsByTag("tr");
        for (Element r : rows) {
            if (r.child(0).tagName() != "td")
                continue;
            product = new CMProduct();
            product.setCategory(catName);

            String imgID = r.child(2).getElementsByTag("a").first()
                    .attr("onclick");
            imgID = imgID.substring(16, imgID.indexOf(','));
            product.setImgId(Integer.parseInt(imgID));

            String unit = StringEscapeUtils.unescapeHtml4(r.child(3).html());
            product.setUnit(unit);

            String brand = StringEscapeUtils.unescapeHtml4(r.child(4)
                    .html());
            product.setBrand(brand);

            String model = StringEscapeUtils.unescapeHtml4(r.child(5).html());
            product.setModel(model);

            Float price = Float.parseFloat(r.child(6).html());
            product.setPrice(price);
            String name = StringEscapeUtils.unescapeHtml4(r.child(1).html());
            name = name.replace("" + unit + " " + brand + " " + model, "");
            name = name.replace(catName + " ", "");

            String cmid = r.child(8).child(1).attr("onclick").split(",")[1];
            product.setCmId(Integer.parseInt(cmid));

            if (name.length() != 0) {
                name = name.substring(0, name.length() - 1);
                if (name.charAt(name.length() - 1) == '"')
                    name += " ";
            }

            product.setDescription(name);
            result.add(product);
        }
        return result;
    }

    String getProviderId(Document doc){
        Element select = doc.getElementById("convenio");
        String script = select.attr("onchange");
        script = script.substring(76);
        return script.substring(0,script.indexOf('\''));
    }

    public byte[] getProdImage(String id) {
        String data = downloader
                .doGetString("http://zonasegura.seace.gob.pe/portlet5open.asp?_portletid_=mod_cm_carrito&scriptdo=doViewMostrarImagenProducto&v_cod_doc="
                        + id + "&v_tipo_imagen=1");
        if (data == null) {
            logger.error("Error downloading imgId " + id);
            return null;
        }
        Document doc = Jsoup.parse(data);
        Elements imgs = doc.getElementsByTag("img");
        String img_url = imgs.first().attr("src");
        System.out.println(urlBase + img_url);
        return downloader.doGetBytes(urlBase + img_url);
    }
}
