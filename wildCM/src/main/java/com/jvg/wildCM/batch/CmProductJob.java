package com.jvg.wildCM.batch;

import com.jvg.wildCM.model.CMProduct;
import com.jvg.wildCM.repositories.CMProductRepository;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.MongoItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * Created by COVICEL on 06/03/2015.
 */
@Configuration
public class CmProductJob {

    @Autowired
    MongoTemplate mongoTemplate;
    @Value("${cm.user}")
    private String cmUser;
    @Value("${cm.password}")
    private String cmPassword;
    @Autowired
    private CMProductRepository repository;

    @Bean
    public ItemReader<CMProduct> cmProductItemReader() {
        CMueServiceItemReader reader = new CMueServiceItemReader(cmUser, cmPassword);
        return reader;
    }

    @Bean
    public ItemProcessor<CMProduct, CMProduct> cmProductItemProcessor() {
        return item -> {
            CMProduct p0 = repository.findByCmId(item.getCmId());
            if (p0 != null)
                item.setId(p0.getId());
            return item;
        };
    }

    @Bean
    public ItemWriter<CMProduct> writer0() throws Exception {

        MongoItemWriter<CMProduct> mongoItemWriter = new MongoItemWriter<>();
        mongoItemWriter.setTemplate(mongoTemplate);
        mongoItemWriter.setCollection("cMProduct");

        return mongoItemWriter;
    }


    @Bean
    public Step stepCmProduct(StepBuilderFactory stepBuilderFactory, ItemReader<CMProduct> reader,
                              ItemWriter<CMProduct> writer, ItemProcessor<CMProduct, CMProduct> processor) {
        return stepBuilderFactory.get("stepCmProduct")
                .<CMProduct, CMProduct>chunk(50)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .listener(new StepExecutionListener() {
                    @Override
                    public void beforeStep(StepExecution stepExecution) {
                        System.out.println("Downloading CMProducts");
                    }

                    @Override
                    public ExitStatus afterStep(StepExecution stepExecution) {
                        //System.out.println("Ensuring index Brand");
                        return null;
                    }
                })
                .build();
    }

}
