package com.jvg.wildCM.batch;

import com.jvg.wildCM.CMueService;
import com.jvg.wildCM.model.CMCategory;
import com.jvg.wildCM.model.CMProduct;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.*;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by JoséLuis on 06/03/2015.
 */
public class CMueServiceItemReader implements ItemStreamReader<CMProduct> {

    static final Logger logger = LogManager.getLogger(CMueServiceItemReader.class.getName());
    CMueService service = new CMueService();
    boolean loggedIn = false;
    List<CMCategory> cmCategories;
    List<CMProduct> products;
    int productIdx = 0;
    int categoryIdx = 0;
    int c = 0;

    public CMueServiceItemReader(String user, String password) {
        loggedIn = service.Login(user, password);
        if (loggedIn) {
            cmCategories = service.getCmCategories();
            products = new LinkedList<>();
            logger.info(user + " Login successful!");
        }
        else{
            logger.error("Login failed");
            service.end();
        }

    }

    @Override
    public CMProduct read() throws Exception, ParseException, NonTransientResourceException {
        if (!loggedIn) throw new IllegalStateException("Service not logged in.");
        if (productIdx < products.size()) {
            return products.get(productIdx++);
        } else {
            if (categoryIdx < cmCategories.size()) {
                CMCategory cat = cmCategories.get(categoryIdx++);
                List<CMProduct> p = service.getCMProductsByCategory(cat);
                if(p!=null) {
                    productIdx = 0;
                    logger.info(cat.getName() + " : " + products.size() + " registros (" + (int) (((double) categoryIdx / (double) cmCategories.size()) * 100) + "%)");
                    if (!products.isEmpty()) c++;
                    products = p;
                }
                return read();
            } else {
                //logger.info("Total : " + result.size() + " productos en " + totalCats + "/" + categories.size() + " categorias")
                service.end();
                return null;
            }
        }
    }

    @Override
    public void open(ExecutionContext executionContext) throws ItemStreamException {

    }

    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {

    }

    @Override
    public void close() throws ItemStreamException {

    }
}
