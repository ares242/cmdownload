package com.jvg.wildCM.model;

/**
 * Created by JoséLuis on 06/03/2015.
 */
public class CMCategory {

    String cmID;
    String Name;

    public String getCmID() {
        return cmID;
    }

    public void setCmID(String cmID) {
        this.cmID = cmID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
