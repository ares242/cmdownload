package com.jvg.wildCM;


import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.jvg.wildCM.model.CMProduct;
import com.jvg.wildCM.repositories.CMProductRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.*;
import java.util.List;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class App implements CommandLineRunner
{
    private static final Logger logger = LogManager.getLogger(App.class.getName());
    @Value("${cm.user}")
    private String cmUser;
    @Autowired
    private CMProductRepository repository;

    public static void main( String[] args )
    {
        SpringApplication.run(App.class, args);
        //System.out.println( "Hello World!" );
    }

    @Override
    public void run(String... args) throws Exception {
        logger.info("Saving to CSV.");
        List<CMProduct> products = repository.findAll();
        saveCMProductsToCSV(products, cmUser  + "_2015_CMuePrices.csv");
    }

    void saveCMProductsToCSV(List<CMProduct> products, String fileName) {
        // create mapper and schema
        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = mapper.schemaFor(CMProduct.class).withHeader();
        schema = schema.withColumnSeparator(',');
        ObjectWriter myObjectWriter = mapper.writer(schema);
        File tempFile = new File(fileName);
        FileOutputStream tempFileOutputStream;
        try {
            tempFileOutputStream = new FileOutputStream(tempFile);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(tempFileOutputStream, 1024);
            OutputStreamWriter writerOutputStream = new OutputStreamWriter(bufferedOutputStream, "UTF-8");
            myObjectWriter.writeValue(writerOutputStream, products);
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
/*
    static int saveProductsImgs(CMueService service, List<CMProduct> products, String prefix_path, boolean overwrite) {
        File dir = new File(prefix_path);
        if (!dir.exists()) {
            if (!dir.mkdirs()) return 0;
        }
        int img_count = 0;
        for (CMProduct p : products) {
            Integer id = p.getImgId();
            File f = new File(prefix_path + id + ".jpg");
            if (!f.exists() || overwrite) {
                byte[] img = service.getProdImage(id.toString());
                try {
                    if (img != null) {
                        img_count++;
                        FileOutputStream fos = new FileOutputStream(f);
                        fos.write(img);
                        fos.close();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        return img_count;
    }*/
}
