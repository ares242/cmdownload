package com.jvg.wildCM.repositories;

import com.jvg.wildCM.model.CMProduct;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
//import org.springframework.stereotype.Repository;

/**
 * Created by COVICEL on 05/03/2015.
 */
//@Repository
public interface CMProductRepository extends MongoRepository<CMProduct, ObjectId> {
    public CMProduct findByCmId(int cmid);
}
