package com.jvg.wildCM.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;

/**
 * Created by COVICEL on 06/03/2015.
 */
@Configuration
@EnableBatchProcessing
@ComponentScan("com.jvg.wildCM.batch")
@PropertySources({@PropertySource("classpath:db.properties"), @PropertySource("classpath:cm.properties")})
public class BatchJobConfig {

    @Autowired
    Step stepCmProduct;

    @Bean
    public Job importItemsJob(JobBuilderFactory jobs) {

        return jobs.get("importItemsJob")//.repository(jobRespository())
                .incrementer(new RunIdIncrementer())
                .start(stepCmProduct)
                        //.next(stepCategory)
                        //.next(stepProduct)
                .build();
    }

}
