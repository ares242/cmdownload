package com.jvg.wildCM.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;

/**
 * Created by COVICEL on 19/01/2015.
 */

@Configuration
@EnableBatchProcessing
@ComponentScan(basePackages = {"com.jvg.wildCM.config","com.jvg.wildCM.batch"})
@PropertySources({@PropertySource("classpath:db.properties"), @PropertySource("classpath:cm.properties")})
public class AppConfig {

    @Autowired
    Step stepCmProduct;

    @Bean
    public Job importItemsJob(JobBuilderFactory jobs) {


        return jobs.get("importBrandJob")
                .incrementer(new RunIdIncrementer())
                .start(stepCmProduct)
                //.start(stepBrand)
                //.next(stepCategory)
                //.next(stepProduct)
                .build();
    }


}
